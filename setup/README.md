Setting up for the course
=====================

Get yourself a Bitbucket account
--------------------------------

Create account if not done already ([bitbucket.org](https://bitbucket.org/)).

Free Individual account is more than enough for this course.

Note that your username will be part of your repository URLs, so choose wisely.


Create code repository
----------------------

You will need to create a separate repository for this course.
Choose whatever name you like, for example, 'appcrypto'.

This repository must be private and must use Git version control.

![Create repository](create_repo.png)


Grant read access
-----------------

Grant the user '**appcrypto@cybersec.ee**' (utappcrypto) read access to your repository:

    Repository settings > User and group access

Type 'appcrypto@cybersec.ee', press 'Enter'.

![Grant access](grant_access.png)

After the access is granted (and the invitation is accepted),
the email address appcrypto@cybersec.ee will resolve to "Applied Cryptography UT".

![Invitation accepted](invitation_accepted.png)

If the invitation is not accepted (should be automatically accepted in a few minutes),
please check that your existing repositories do not exceed the amount of collaborators (5 on the free plan).
If they do, the invitation can not be accepted and you will recieve no error indicating that.

Add repository to the grading page
----------------------------------

After your invitation is accepted, add your repository URL together with student ID to the grading page at [https://cybersec.ee/appcrypto2021/](https://cybersec.ee/appcrypto2021/).

Start watching the course repository
-------------------------------------

Open the course repository page [https://bitbucket.org/appcrypto/2021/](https://bitbucket.org/appcrypto/2021/).

Click on the "..." on the right side of the course repository page and then "Manage notifications". Then "Watch this repository".

![Watch this repository](watch_repository.png)

Install Git client
------------------------

Command for Ubuntu:

    sudo apt install git

Learn how to use the client to perform basic operations.

Note that there are several GUI clients, but it is
strongly recommend *not* to use them until you get really comfortable with
command-line client and understand how it works.


Make your first commit
----------------------

Create a file called 'hello.txt' with the following content:

    My first commit

Commit and push to your repository.
Verify that it appears in the web interface under 'source' tab.

All done.

---
